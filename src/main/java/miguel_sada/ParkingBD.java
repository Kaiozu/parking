package miguel_sada;

	


	import java.sql.*;

	public class ParkingBD implements Parking {

		private static Connection conn = null;
		static int cont = 0;
		static float precioTotal = 0;
		
		public static void conectar() throws ClassNotFoundException, SQLException {
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:parking.db");
			if (conn != null) {
				conn.setReadOnly(false);
				System.out.println("Conexi�n con la base de datos.");
			}
		}
		
		public static void cerrarConexion() throws SQLException {
			conn.close();
		}

		public void aparcar(Coche coche) throws SQLException {
			PreparedStatement stat = conn.prepareStatement("INSERT INTO coche VALUES (?,?,?)");
			stat.setInt(1, coche.getId());
			stat.setString(2, coche.getMatricula());
			stat.setFloat(3, coche.getPrecio());
			stat.executeUpdate();
		}

		public void buscarMatricula(String matricula) throws SQLException {
			ResultSet rs = null;
			Statement stat = conn.createStatement();
			rs = stat.executeQuery("SELECT * FROM coche WHERE matricula = ?");
			((PreparedStatement) stat).setString(1, matricula);
			while (rs.next()) {
				System.out.println("ID = " + rs.getString("id") + ", Matricula = " + rs.getString("matricula") + ", Precio = " + rs.getString("precio"));
				cont++;
			}
			rs.close();
		}

		public void mostrarEstadoDelParking() throws SQLException {
			ResultSet rs = null;
			Statement stat = conn.createStatement();
			rs = stat.executeQuery("select * from coche");
			while (rs.next()) {
				System.out.println("ID = " + rs.getString("id") + ", Matricula = " + rs.getString("matricula") + ", Precio = " + rs.getString("precio"));
				cont++;
			}
			rs.close();
		}

		public float calcularValorTotal() throws SQLException {
			ResultSet rs = null;
			Statement stat = conn.createStatement();
			rs = stat.executeQuery("select * from coche");
			while (rs.next()) {
				precioTotal += + rs.getFloat("precio");
				cont++;
			}
			System.out.println(precioTotal);
			rs.close();
			return precioTotal;
		}

		public void buscarPorMatricula(String matricula) {
			// TODO Auto-generated method stub
			
		}
		
	}
	

