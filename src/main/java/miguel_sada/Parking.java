package miguel_sada;

import java.sql.SQLException;

public interface Parking {
	
	public void aparcar(Coche coche) throws SQLException;
	
	public void buscarPorMatricula(String matricula) throws SQLException;
	
	public void mostrarEstadoDelParking() throws SQLException;
	
	public float calcularValorTotal() throws SQLException;
	

}
