package miguel_sada;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * Clase principal que prueba el comportamiento del 
 * parking en Base de Datos.
 *
 */
public class App 
{
	/**
	 * Punto de entrada de la aplicación.
	 * @param args
	 * @throws SQLException 
	 */
    public static void main( String[] args ) throws SQLException
    {
    	ParkingBD parking = new ParkingBD();
    	Scanner console = new Scanner(System.in);
    	Coche coche = null;
		int opc = 0;
		try {
			parking.conectar();
		} catch (ClassNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		do {
			System.out.println("1. Añadir coche");
			System.out.println("2. Buscar por matrícula");
			System.out.println("3. Mostrar parking");
			System.out.println("4. Calcular total");
			System.out.println("5. Salir");
			opc = Integer.parseInt(console.nextLine());
			switch (opc) {
			case 1:
				System.out.println("Introduzca la matrícula:");
				String matricula = console.nextLine();
				System.out.println("Introduzca el precio:");
				Float precio = Float.parseFloat(console.nextLine());
				coche = new Coche(matricula, precio);
				parking.aparcar(coche);
				break;
			case 2:
				System.out.println("Introduzca la matrícula a buscar:");
				matricula = console.nextLine();
				parking.buscarPorMatricula(matricula);
				break;
			case 3:
				parking.mostrarEstadoDelParking();
				break;
			case 4:
				float total =parking.calcularValorTotal();
				System.out.println("El valor total de los vehículos: "+total);
				break;
			case 5:
				try {
					parking.cerrarConexion();
				} catch (SQLException e) {
					System.err.println(e.getMessage());
				}
				break;
			}
		}while (opc != 5);
    }
}
