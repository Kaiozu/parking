package miguel_sada;

import java.util.ArrayList;

public class ParkingMemoria implements Parking{

	ArrayList<Coche> coches = new ArrayList<Coche>();
	float precioTotal = 0;

	public void aparcar(Coche coche) {
		// TODO Auto-generated method stub
		coches.add(coche);
	}

	public void buscarPorMatricula(String matricula) {
		// TODO Auto-generated method stub
		for (Coche coche : coches) {
			if (coche.matricula == matricula) {
				System.out.println("ID = "+coche.id+", Matricula: "+coche.matricula+", Precio ="+coche.precio);
			}
		}
		
	}

	public void mostrarEstadoDelParking() {
		// TODO Auto-generated method stub
		for (Coche coche : coches) {
			System.out.println("ID = "+coche.id+", Matricula: "+coche.matricula+", Precio ="+coche.precio);
		}
	}

	public float calcularValorTotal() {
		// TODO Auto-generated method stub
		for (Coche coche : coches) {
			precioTotal += coche.precio;
			return precioTotal;
		}
		return 0;
	}

	
	
	
	
}
