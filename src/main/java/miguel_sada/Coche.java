package miguel_sada;

public class Coche {
	
	int id;
	String matricula;
	float precio;
	
	public Coche() {
		super();
	}
	public Coche(String matricula, float precio) {
		super();
		this.matricula = matricula;
		this.precio = precio;
	}
	public Coche(int id, String matricula, float precio) {
		super();
		this.id = id;
		this.matricula = matricula;
		this.precio = precio;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public float getPrecio() {
		return precio;
	}
	public void setPrecio(float precio) {
		this.precio = precio;
	}
	
	@Override
	public String toString() {
		return "Coche [id=" + id + ", matricula=" + matricula + ", precio=" + precio + "]";
	}
	
	
	
	

}
